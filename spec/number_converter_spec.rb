require_relative "../lib/number_converter.rb"

def test_conversion_hash hsh
	hsh.each_pair do |k,v|
		expect(NumberConverter.convert(k)).to eq(v)	
		puts "#{k} : #{NumberConverter.convert(k)}"
	end
end

describe "single digits" do

	it "should convert single digits up to 10" do
		test_conversion_hash(
			1 => "one", 2=> "two", 3=> "three", 4=> "four",
			5=> "five", 6=> "six", 7=> "seven", 8=> "eight",
			9=> "nine" , 10=> "ten",

			11 => "eleven", 15 => "fifteen", 19 => 'nineteen'
		)
	end	

	it "should properly compose between 20-99" do
		test_conversion_hash({
			20 => "twenty",
			22 => "twenty-two",
			35 => "thirty-five",
			57 => "fifty-seven",
			99 => "ninety-nine"
			})
	end

	it "should properly compose between 100-1999" do
		test_conversion_hash({
			100 => "one hundred",
			291 => "two hundred and ninety-one",
			987 => "nine hundred and eighty-seven",
			1456 => "one thousand four hundred and fifty-six",
			1999 => "one thousand nine hundred and ninety-nine"
			})
	end

	it "should properly compose between 2000-9999" do
		test_conversion_hash({
			2000 => "two thousand",
			2001 => "two thousand and one",
			3598 => "three thousand five hundred and ninety-eight",
			5701 => "five thousand seven hundred and one",
			9999 => "nine thousand nine hundred and ninety-nine"
			})
	end
	it "should properly compose between 10000-999999" do
		test_conversion_hash({
			10001 => "ten thousand and one",
			99999 => "ninety-nine thousand nine hundred and ninety-nine",
			999999 => "nine hundred ninety-nine thousand nine hundred and ninety-nine"
			})
	end
	it "should properly compose between 1000000-99000000" do
		test_conversion_hash({
			4000000 => "four million",
			3687365 => "three million six hundred eighty-seven thousand three hundred and sixty-five"
			})		
	end
	it "should properly compose billions" do
		test_conversion_hash({
			2053037489 => "two billion fifty-three million thirty-seven thousand four hundred and eighty-nine",
			235053037489 => "two hundred thirty-five billion fifty-three million thirty-seven thousand four hundred and eighty-nine"
			})
	end
end