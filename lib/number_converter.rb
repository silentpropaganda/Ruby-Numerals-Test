module NumberConverter

	SINGLE_DIGITS = %w(
			zero one two three four five six seven eight nine
			ten eleven twelve
			thirteen fourteen fifteen sixteen seventeen eighteen nineteen
			)
	TENS = %w(twenty thirty fourty fifty sixty seventy eighty ninety)
	


	# converts a number to its english representation
	def self.convert input
		raw_out = convert_billions(input).flatten
		# Append an "and" to the last position if necessary
		if raw_out.size > 1
			raw_out.insert(-2, "and")
		end
		raw_out.join(" ")
	end

	def self.convert_billions input
		validate_input_range(input, 0, 1000000000000)
		unified_conversion( input, :convert_millions, 1000000000, 1000000000, "billion", :convert_hundreds)
	end	

	def self.convert_millions input		
		unified_conversion( input, :convert_thousands, 1000000, 1000000, "million", :convert_hundreds)
	end	

	def self.convert_thousands input		
		unified_conversion( input, :convert_hundreds, 1000, 1000, "thousand", :convert_hundreds)
	end	


	def self.convert_hundreds input		
		unified_conversion( input, :convert_double_digits, 100, 100, "hundred")
	end


	# Generic conversion routine
	#
	# Parameters:
	#   +input+   The input number to convert
	#   +lower_level_method+	The next method in the chain to call
	#   +lower_bound+			The upper bound of the conversion for the
	# 							lower_level_method
	#   +divider+				The divider used in the current level (like 100, 1000)
	#   +scale_text+ 			The text to append as the scale (like "hundred", "thousand")
	#
	# Returns: an array containing each part as a Matroshka doll.
	def self.unified_conversion(
			input,
			lower_level_method, lower_bound,
			divider, scale_text,
			base_conversion_method = :convert_single_digit
			)
		return self.send(lower_level_method, input) if input < lower_bound

		first_digits = self.send(base_conversion_method, input / divider)
		o = [ "#{first_digits.join(" ")} #{scale_text}" ]
		last_digits = input % divider
		o += [self.send(lower_level_method, last_digits)] if last_digits != 0
		o
	end


	# Converts a two digits to its textual representation.
	def self.convert_double_digits input		
		return convert_single_digit(input) if input < 20
		
		o = [ TENS[(input / 10)-2] ]
		last_digit = input % 10
		o << convert_single_digit(last_digit) if last_digit != 0
		[o.join("-")]
	end


	# Converts a single digit (english digit, up to 19) to its
	# textual representation.
	def self.convert_single_digit input		
		[SINGLE_DIGITS[input]]
	end



	# Validates that +input+ is in the range of [min, max),
	# Throws an error when input is out of range.
	def self.validate_input_range input, min, max
		unless input < max && input >= min
			raise "Bad input for conversion: #{input} is not in [#{min}, #{max})"
		end
	end

end